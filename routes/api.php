<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/forgotPassword', 'App\Http\Controllers\UserController@forgotPassword');
Route::post('/resetPassword', 'App\Http\Controllers\UserController@resetPassword');
Route::post('/createUser', 'App\Http\Controllers\UserController@createUser');

