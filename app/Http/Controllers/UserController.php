<?php

namespace App\Http\Controllers;

use App\Models\User;
use Ichtrojan\Otp\Otp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    /*
     * create user for test
     */
    public function createUser(){
        $user = \App\Models\User::create([
            'name'=>'firas',
            'email'=>'firaskanaan93@gmail.com',
            'password'=>Hash::make('12345678')
        ]);
        return $user;
    }
    /*
     * forgot password request
     */
    public function forgotPassword(Request $request) {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);
        if ($validator->fails())
            return  response()->json(["msg" => 'Missing Parameter']);

        $user=User::where('email',$request->get('email'))->first();
        if(empty($user))
            return  response()->json(["msg" => 'User not found']);

        $otp = rand(1000,9999);
        $user = User::where('email','=',$request->get('email'))->update(['otp' => $otp]);
        $data = array('otp'=>$otp);

        Mail::send('mail', $data, function($message)use($request) {
            $message->from('info@app.com', 'Your Application');

            $message->to($request->get('email'))->subject('Reset password code');
        });

        return response()->json(["msg" => 'Reset password code sent on your email .']);
    }

    /*
     * reset password after code receive
     */
    public function resetPassword(Request $request) {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'otp' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails())
            return  response()->json(["msg" => 'Missing Parameter']);

        $user=User::where('email',$request->get('email'))->where('otp',$request->get('otp'))->first();
        if(empty($user))
            return  response()->json(["msg" => 'code not correct']);

        $user->password = Hash::make($request->get('password'));
        $user->otp=null;
        $user->save();

        return response()->json(["msg" => "Password has been successfully changed"]);
    }
}
